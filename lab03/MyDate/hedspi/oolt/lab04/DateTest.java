package hedspi.oolt.lab04;

public class DateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyDate date1 = new MyDate();
		date1.print();
		MyDate date2 = new MyDate(22,8,2001);
		date2.print();
		MyDate date3 = new MyDate("August 22nd 2001");
		date3.print();
		//
		date2.setDay(32);
		date2.setMonth(13);
		date2.setYear(-1);
		date2.print();
		//
		date1.accept();
		System.out.println(date1.getDay() + "-" + date1.getMonth() +"-"+ date1.getYear());
		//
	}

}
