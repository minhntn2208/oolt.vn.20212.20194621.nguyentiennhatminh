package hedspi.oolt.lab04;
import java.util.Scanner;
public class MyDate {
	//Attribute
	private int day;
	private int month;
	private int year;
	//Method
	//Constructor
	public MyDate() {
		this.day = 1;
		this.month = 1;
		this.year = 1900;
	}
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String date) {
		char day[] = new char[10];
		char month[] = new char[10];
		char year[] = new char[10];
		String[] FullMonth = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
		int iter = 0,iter1 = 0, iter2 = 0;
		while(date.charAt(iter) != ' ') {
			month[iter] = date.charAt(iter);
			iter++;
		}
		month[iter] = '\0';
		String monthString = new String(month).split("\0")[0];
		for(int i = 0; i < FullMonth.length; i++) {
			if(monthString.equals(FullMonth[i])) {
				this.month = i + 1;
			}
		}
		iter++;
		while(date.charAt(iter) != ' ') {
			if(date.charAt(iter)>= '0' && date.charAt(iter)<= '9') {
				day[iter1] = date.charAt(iter);
				iter1++;
			}
			iter++;
		}
		day[iter1] = '\0';
		iter++;
		this.day = Integer.parseInt(new String(day).split("\0")[0]);
		while(iter < date.length()) {
			year[iter2] = date.charAt(iter);
			iter++; iter2++;
		}
		year[iter2] = '\0';
		this.year = Integer.parseInt(new String(year).split("\0")[0]);
	}
	//setter and getter
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if(this.month == 1 || this.month == 3 || this.month == 5 || this.month == 7 
				|| this.month == 8 || this.month == 10 || this.month == 12) {
			if(day >= 1 && day <= 31) {
				this.day = day;
			}
			else
				this.day = 1;
		}
		else if(this.month == 4 || this.month == 6 || this.month == 9 || this.month == 11){
			if(day >= 1 && day <= 30) {
				this.day = day;
			}
			else
				this.day = 1;
		}
		else {
			if( (this.year % 4 == 0) && (this.year % 100 != 0)  || (this.year % 400 == 0)) {
				if(day >= 1 && day <= 29) {
					this.day = day;
				}
				else
					this.day = 1;
			}
			else {
				if(day >= 1 && day <= 28) {
					this.day = day;
				}
				else
					this.day = 1;
			}
		}
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		if(month >= 1 && month <= 12) {
			this.month = month;
		}
		else
			this.month = 1;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		if(year > 0) {
			this.year = year;
		}
		else
			this.year = 0;
	}
	//utils method
	public void accept() {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the date:");
		String date = sc.nextLine();
		
		MyDate temp = new MyDate(date);
		this.day = temp.getDay();
		this.month = temp.getMonth();
		this.year = temp.getYear();
	}
	public void print() {
		System.out.println(this.day+ "-" + this.month +"-"+ this.year);
	}
	
}
