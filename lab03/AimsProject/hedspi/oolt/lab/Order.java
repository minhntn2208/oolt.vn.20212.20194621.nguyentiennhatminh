package hedspi.oolt.lab;

public class Order {
	//const maxOrder
	public static final int MAX_NUMBERS_ORDERED = 10;
	//array of dvds
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered = 0;
	//get / set
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	//
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(this.qtyOrdered == MAX_NUMBERS_ORDERED)
			System.out.println("The order is almost full");
		else {
			this.itemsOrdered[qtyOrdered] = disc;
			qtyOrdered++;
			System.out.println("The disc has been added");
			System.out.println("Total disc: " + this.qtyOrdered);
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		//check if empty
		boolean check = false;
		if(qtyOrdered != 0) {
			for(int i = 0; i < this.qtyOrdered; i++) {
				if(itemsOrdered[i] == disc) {
					check = true;
				}
				if(check == true) {
					itemsOrdered[i] = itemsOrdered[i+1];
				}
			}
			if(check == true) {
				qtyOrdered--;
			}
			else {
				System.out.println("Found no such digital video disc in the order");
			}
		}
		else {
			System.out.println("Have no disc in the order");
		}
	}
	public float totalCost() {
		float total = 0.0f;
		for(int i = 0; i < this.qtyOrdered; i++) {
			total += itemsOrdered[i].getCost();
		}
		return total;
	}
	
}
