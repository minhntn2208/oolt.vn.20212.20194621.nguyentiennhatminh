package hedspi.oolt.lab;

public class Aims {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Star War", "SF", "George Lucas", 124, 24.95f);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Turning Red", "ComingOfAge", "DomeShi", 100, 30.5f);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Eternals","Superhero","Chloe Zhao",180,50.5f);
		Order anOrder = new Order();
		anOrder.addDigitalVideoDisc(dvd1);
		anOrder.addDigitalVideoDisc(dvd2);
		anOrder.addDigitalVideoDisc(dvd3);
		System.out.println("Total cost: " + anOrder.totalCost());
		anOrder.removeDigitalVideoDisc(dvd1);
		System.out.println("After remove disk1, total cost: " + anOrder.totalCost());
		anOrder.removeDigitalVideoDisc(dvd1);
		System.out.println("Try to remove disk1 again, total cost: " + anOrder.totalCost());
		anOrder.removeDigitalVideoDisc(dvd2);
		System.out.println("Remove disk2, total cost: " + anOrder.totalCost());
		anOrder.removeDigitalVideoDisc(dvd3);
		System.out.println("Remove disk3, total cost: " + anOrder.totalCost());
		anOrder.removeDigitalVideoDisc(dvd3);
		System.out.println("Try to remove disk3, total cost: " + anOrder.totalCost());
	}

}
