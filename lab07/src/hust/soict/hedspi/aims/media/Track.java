package hust.soict.hedspi.aims.media;

public class Track implements Playable{
	private String title;
	private int length;
	public Track() {
		// TODO Auto-generated constructor stub
		this.title = "";
		this.length = 0;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public void play() {
		System.out.println("Play Track: " + this.getTitle());
		System.out.println("Track length: " + this.getLength());
	}

}
