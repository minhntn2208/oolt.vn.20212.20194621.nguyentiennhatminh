package hust.soict.hedspi.test.utils;

import hust.soict.hedspi.aims.utils.DateUtils;
import hust.soict.hedspi.aims.utils.MyDate;

public class DataTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyDate date = new MyDate(22,8,2001);
		MyDate date7 = new MyDate("second","September","two thousand fourty");
		MyDate date8 = new MyDate("second","September","nineteen fourty");
		MyDate date9 = new MyDate("second","September","nineteen fourty one");
		MyDate date10 = new MyDate("second","September","one thousand fourty three");
		date7.print(1);
		date8.print(2);
		date9.print(3);
		date10.print(5);
		//Sort a date array
		System.out.println("Sort a date array: ");
		MyDate date1 = new MyDate(21,8,2001);
		MyDate date2 = new MyDate(22,8,2002);
		MyDate date3 = new MyDate(22,9,2001);
		MyDate date4 = new MyDate(1,1,2000);
		MyDate date5 = new MyDate(4,8,2002);
		MyDate[] dates = {date,date1,date2,date3,date4,date5,date7,date8,date9,date10};
		DateUtils.sortDate(dates);
		for(int i =0; i < dates.length; i++) {
			dates[i].print(4);
		}
	}

}
