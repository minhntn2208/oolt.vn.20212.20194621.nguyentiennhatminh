package hust.soict.hedspi.aims.order;

import java.util.Random;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.utils.MyDate;
import java.util.AbstractList;
import java.util.ArrayList;
public class Order {
	//const maxOrder
	public static final int MAX_NUMBERS_ORDERED = 10;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private int qtyOrdered = 0;
	
	private MyDate dateOrdered;
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;
	protected static int id = 0;
	//get / set
	public Order() {
		this.setId();
	}
	public Order(MyDate date){
		if(this.nbOrders < MAX_LIMITTED_ORDERS) {
			this.dateOrdered = date;
			this.nbOrders++;
		}else {
			throw new ArithmeticException("Exceed the number of orders can created");   
		}
	}
//	public int getQtyOrdered() {
//		return qtyOrdered;
//	}
//	public void setQtyOrdered(int qtyOrdered) {
//		this.qtyOrdered = qtyOrdered;
//	}
	
	public static int getId() {
		return id;
	}
	public static void setId() {
		Order.id = 0;
	}
	public void addMedia(Media media) {
		if(!this.itemsOrdered.contains(media)) {
			this.id++;
			media.setId(this.id);
			this.itemsOrdered.add(media);
		}
	}
	public void removeMedia(Media media) {
		if(this.itemsOrdered.contains(media)) {
			this.itemsOrdered.remove(media);
		}
	}

	public void removeMedia(int id) {
		Media mediaItem;
		java.util.Iterator iter = itemsOrdered.iterator();
		while (iter.hasNext()) {
			mediaItem = (Media) (iter.next());
			if(mediaItem.getId() == id) {
				removeMedia(mediaItem);
			}
			else {
				System.out.println("No itemID result return");
			}
		}

	}
	public float totalCost() {
		float total = 0.0f;
		Media mediaItem;
		java.util.Iterator iter = itemsOrdered.iterator();
		while (iter.hasNext()) {
			mediaItem = (Media) (iter.next());
			total += mediaItem.getCost();
		}
		return total;
	}
	public void printOrder() {
		System.out.println("********************Order*********************");
		Media mediaItem;
		java.util.Iterator iter = itemsOrdered.iterator();
		while (iter.hasNext()) {
			mediaItem = (Media) (iter.next());
			mediaItem.printInfo();
		}
		System.out.println("\nTotal cost: " + totalCost());
		System.out.println("**********************************************");
		
	}
	
	public Media getALuckyItem() {
		Random rd = new Random();
		int luckynumber = rd.nextInt(this.itemsOrdered.size());
		itemsOrdered.get(luckynumber).setCost(0);;
		return itemsOrdered.get(luckynumber);
	}
}
