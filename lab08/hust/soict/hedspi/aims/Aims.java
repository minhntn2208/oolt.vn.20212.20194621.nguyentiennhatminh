package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.Track;
import hust.soict.hedspi.aims.order.Order;
import hust.soict.hedspi.aims.utils.MyDate;
import java.util.Scanner;
public class Aims {
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	public static void main(String[] args) {
//		Thread memCon = new Thread(new MemoryDaemon(), "Memory Observation");
//		memCon.setDaemon(true);
//		memCon.start();
//		// TODO Auto-generated method stub
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Star War", "SF", "George Lucas", 124, 24.95f);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Turning Red", "ComingOfAge", "DomeShi", 100, 30.5f);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Eternals","Superhero","Chloe Zhao",180,50.5f);
//		int choice;
//		Scanner sc = new Scanner(System.in);
//		Order newOrder = null;
//		do {
//			showMenu();
//			choice = sc.nextInt();
//			switch(choice) {
//			case 1:
//				newOrder = new Order();
//				break;
//			case 2:
//				if(newOrder != null) {
//					System.out.println("Please insert the tittle you want: ");
//					String title = sc.next();
//					sc.nextLine();
//					System.out.println("Please insert the category: ");
//					String category = sc.next();
//					System.out.println("Item price: ");
//					float cost = sc.nextFloat();
//					Media media = new Media(title,category,cost);
//					newOrder.addMedia(media);
//				}
//				else {
//					System.out.println("No order have been created");
//				}
//				break;
//			case 3:
//				System.out.println("Please insert the item id you want to remove: ");
//				int id = sc.nextInt();
//				newOrder.removeMedia(id);
//				break;
//			case 4:
//				newOrder.printOrder();
//				break;
//			case 0:
//				break;
//			default:
//				break;
//			}
//		}while(choice != 0);
//		System.out.println("Exit");
//		sc.close();
//	}
		java.util.Collection collection = new java.util.ArrayList(); 
		collection.add(dvd2);
		collection.add(dvd1);
		collection.add(dvd3);
		
		java.util.Iterator iterator = collection.iterator();
		System.out.println("---------------------");
		System.out.println("The DVDs currently in the order are: ");
		while(iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		java.util.Collections.sort((java.util.List)collection);
		
		iterator = collection.iterator();
		
		System.out.println("---------------------");
		System.out.println("The DVDsin sorted order are: ");
		while(iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		System.out.println("---------------------");
	}
}
