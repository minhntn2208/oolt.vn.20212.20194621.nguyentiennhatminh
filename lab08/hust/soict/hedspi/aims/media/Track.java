package hust.soict.hedspi.aims.media;

public class Track implements Playable, Comparable{
	private String title;
	private int length;
	public Track() {
		// TODO Auto-generated constructor stub
		this.title = "";
		this.length = 0;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public void play() {
		System.out.println("Play Track: " + this.getTitle());
		System.out.println("Track length: " + this.getLength());
	}
	public boolean equals(Object o) {
		if(o == null) return false;
		if(o instanceof Track) {
			Track track2 = (Track)o;
			return (this.getTitle().equals(track2.getTitle())) && (this.length == track2.length);
		}
		return false;
	}
	
	public int compareTo(Object o) {
		return this.title.compareTo(((Track)o).getTitle());
	}
}
