package hust.soict.hedspi.aims.media;

public abstract class Media implements Comparable<Media>{
	protected String title;
	protected String category;
	protected float cost;
	protected int id;
	public Media() {
		// TODO Auto-generated constructor stub
	}
	public Media(String title){
		this.title = title;
	}
	public Media(String title, String category){
		this(title);
		this.category = category;
	}
	public Media(String title, String category, float cost){
		this(title, category);
		this.cost = cost;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void printInfo() {
		System.out.print(this.title + " - ");
		System.out.print(this.category + " - ");
		System.out.print(this.id + " - ");
		System.out.println(this.cost);
	}
	public boolean equals(Object o) {
		if(o == null) return false;
		if(o instanceof Media) {
			return ((Media)o).getId() == this.id;
		}
		return false;
	}
	
	public int compareTo(Media obj) {
		if(this.cost > obj.getCost()) return 1;
		else if(this.cost < obj.getCost()) return -1;
		else return 0;
	}
}
