package hust.soict.hedspi.aims.media;
import java.util.ArrayList;
public class CompactDisc extends Disc implements Playable{
	private String artist;
	private ArrayList<Track> tracks = new ArrayList<>();
	public CompactDisc() {
		// TODO Auto-generated constructor stub
	}
	public String getArtist() {
		return artist;
	}
	public int getLength(ArrayList<Track> tracks) {
		int sumLength = 0;
		for(int i = 0; i< tracks.size(); i++) {
			sumLength += tracks.get(i).getLength();
		}
		super.length = sumLength;
		return super.length;
	}
	
	public void addTrack(Track track) {
		if(tracks.contains(track)) {
			System.out.println("Already exist");
		}
		else {
			this.tracks.add(track);
		}
	}
	
	public void removeTrack(Track track) {
		if(tracks.contains(track)) {
			this.tracks.remove(track);
		}
		else {
			System.out.println("Not exist");
		}
	}
	public void play() {
		System.out.println("Artist: " + this.getArtist());
		for(int i = 0; i<tracks.size(); i++) {
			tracks.get(i).play();
		}
	}
//	public int compareTo(Object o) {
//		return this.artist.compareTo(((CompactDisc)o).getArtist());
//	}
}
