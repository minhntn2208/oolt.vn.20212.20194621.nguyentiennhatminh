import javax.swing.JOptionPane;
import java.lang.Math;
public class SecondDegreeEqu {
    public static void main(String[] args) {
        String strNum1, strNum2, strNum3;
        strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
        strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
        strNum3 = JOptionPane.showInputDialog(null, "Please input the third number: ", "Input the third number", JOptionPane.INFORMATION_MESSAGE);
        String strNotification = "The result of equation " + strNum1 +"x^2 + " + strNum2 + "x + "+ strNum3 +"= 0 is: ";
        double a = Double.parseDouble(strNum1);
        double b = Double.parseDouble(strNum2);
        double c = Double.parseDouble(strNum3);
        double delta = b*b - 4*a*c;
        if(a == 0){
            double res = -c/b;
            JOptionPane.showMessageDialog(null, strNotification + res);
        }
        if(delta == 0){
            double result = -b / (2*a);
            JOptionPane.showMessageDialog(null, strNotification + "double root x = " +result);
        }
        else if(delta > 0){
            double result1 = (-b + Math.sqrt(delta)) / (2*a);
            double result2 = (-b - Math.sqrt(delta)) / (2*a);
            JOptionPane.showMessageDialog(null, strNotification + "two distict root:" + result1 + " or " + result2);
        }
        else{
            JOptionPane.showMessageDialog(null, strNotification + "no solution");
        }
        System.exit(0);
    }
}
