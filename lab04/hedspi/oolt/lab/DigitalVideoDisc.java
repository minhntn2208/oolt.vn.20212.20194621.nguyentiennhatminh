package hedspi.oolt.lab;

public class DigitalVideoDisc {
	//Atribute
	private String title;
	private String category;
	private String director;
	private int length;
	private float cost;
	
	//Method
	//get/set
	public String getTitle() {
		return this.title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		if(length >= 0)
			this.length = length;
		else
			this.length = 0;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		if(cost > 0)
			this.cost = cost;
		else
			this.cost = 0.0f;
	}
	//constructor
	//create memory contain information of objects and assign value for object 
	//name is the same with class name
	//have no return value
	//can have more than 1 constructor
	
	//constructor with no parameter
	public DigitalVideoDisc() {
		this.title = "";
		this.category = "";
		this.director = "";
		this.length = 0;
		this.cost = 0.0f;
	}
	
	//constructor have parameter
	public DigitalVideoDisc(String title) {
		this.title = title;
	}
	public DigitalVideoDisc(String title, String category) {
		super();
		this.title = title;
		this.category = category;
	}
	public DigitalVideoDisc(String title, String category, String director) {
		super();
		this.title = title;
		this.category = category;
		this.director = director;
	}
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.director = director;
		this.length = length;
		this.cost = cost;
	}
	
	//
	public void printInfo() {
		System.out.println("-------DVD Info-------");
		System.out.println("Title: " + this.title);
		System.out.println("Category: " + this.category);
		System.out.println("Director: " + this.director);
		System.out.println("Length: " + this.length);
		System.out.println("Cost: " + this.cost);
	}
	
	public void printInfo2() {
		System.out.print("DVD - ");
		System.out.print(this.title + " - ");
		System.out.print(this.category + " - ");
		System.out.print(this.director + " - ");
		System.out.print(this.length + " : ");
		System.out.print(this.cost);
	}
}
