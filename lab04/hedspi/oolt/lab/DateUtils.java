package hedspi.oolt.lab;

public class DateUtils {
	public static boolean compareDate(MyDate date1, MyDate date2) {
		if(date1.getYear() > date2.getYear()) {
			return true;
		}
		if(date1.getYear() < date2.getYear()) {
			return false;
		}
		else {
			if(date1.getMonth() > date2.getMonth()) {
				return true;
			}
			if(date1.getMonth() < date2.getMonth()) {
				return false;
			}
			else {
				if(date1.getDay() >= date2.getDay()) {
					return true;
				}
				else {
					return false;
				}
			}
		}
		
	}
	public static void sortDate(MyDate[] dates) {
		for(int i = 0; i < dates.length; i++) {
			for(int j = i + 1; j < dates.length; j++) {
				if(compareDate(dates[i],dates[j])) {
					MyDate temp = dates[i];
					dates[i] = dates[j];
					dates[j] = temp;
				}
			}
		}
	}
	
}
