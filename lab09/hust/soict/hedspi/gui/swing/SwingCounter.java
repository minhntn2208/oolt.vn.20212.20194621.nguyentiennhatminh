package hust.soict.hedspi.gui.swing;

import javax.swing.JFrame;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class SwingCounter extends JFrame {
	private JTextField tfCount;
	private JButton btnCount;
	private int count = 0;
	public SwingCounter() {
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		cp.add(new JLabel("Counter"));
		tfCount = new JTextField("0");
		tfCount.setEditable(false);
		cp.add(tfCount);
		btnCount = new JButton("Count");
		cp.add(btnCount);
		btnCount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				++count;
				tfCount.setText(count + "");
			}
		});
		this.setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Swing Counter");
		setSize(300,100);
		setVisible(true);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new SwingCounter();
			}
		});
	}

}
