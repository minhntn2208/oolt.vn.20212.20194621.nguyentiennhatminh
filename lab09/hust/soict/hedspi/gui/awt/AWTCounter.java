package hust.soict.hedspi.gui.awt;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AWTCounter extends Frame {
	//Khai bao cac component cho giao dien
	private Label lblCount;
	private TextField tfCount;
	private Button btnCount;
	private int count; //bien chua gia tri dem
	
	//Constructor
	public AWTCounter() {
		//Thiet lap layout cho cua so giao dien
		// FlowLayout: bo cuc dang don gian: component nao
		//duoc them vao truoc se xuat hien truoc tren 1 hang
		//neu khong du cho thi xuong hang moi
		this.setLayout(new FlowLayout());
		
		//Khoi tao cac component va them vao giao dien
		lblCount = new Label("Counter");
		this.add(lblCount);
		tfCount = new TextField(count + "", 10); //ep kieu String
		tfCount.setEditable(false);
		this.add(tfCount);
		btnCount = new Button("Count");
		this.add(btnCount);
		//Dang ky lang nghe su kien tren Button
		btnCount.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						++count;
						tfCount.setText(count + "");
					}
				}
				);
		
		//Thiet lap thong tin cho cua so giao dien
		this.setTitle("AWT Counter");
		this.setSize(250, 100);
		this.setVisible(true);
	}
	//3 cach dang ki lang nghe su kien tren componet
	//1. Tao lop thuc thi giao dien listener rieng biet
	//2. Khai bao cho Frame thuc thi giao dien
	//3. anonymous class
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AWTCounter app = new AWTCounter();
	}

}
