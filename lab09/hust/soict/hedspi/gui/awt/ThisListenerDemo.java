package hust.soict.hedspi.gui.awt;

import java.awt.Frame;
import java.awt.*;
import java.awt.event.*;
public class ThisListenerDemo extends Frame implements ActionListener, WindowListener {
	private TextField tfCount;
	private Button btnCount;
	private int count = 0;
	public ThisListenerDemo() {
		setLayout(new FlowLayout());
		
		add(new Label("Counter"));
		
		tfCount = new TextField("0", 10);
		tfCount.setEditable(false);
		add(tfCount);
		
		btnCount = new Button("Count");
		add(btnCount);
		btnCount.addActionListener(this);
		addWindowListener(this);
		
		setTitle("WindowEvent Demo");
		setSize(250, 100);
		setVisible(true);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new ThisListenerDemo();  // Let the construct do the job
	}
	 /* ActionEvent handler */
	   @Override
	   public void actionPerformed(ActionEvent evt) {
	      ++count;
	      tfCount.setText(count + "");
	   }

	   /* WindowEvent handlers */
	   // Called back upon clicking close-window button
	   @Override
	   public void windowClosing(WindowEvent evt) {
	      System.exit(0);  // Terminate the program
	   }

	   // Not Used, BUT need to provide an empty body to compile.
	   @Override public void windowOpened(WindowEvent evt) { }
	   @Override public void windowClosed(WindowEvent evt) { }
	   // For Debugging
	   @Override public void windowIconified(WindowEvent evt) { System.out.println("Window Iconified"); }
	   @Override public void windowDeiconified(WindowEvent evt) { System.out.println("Window Deiconified"); }
	   @Override public void windowActivated(WindowEvent evt) { System.out.println("Window Activated"); }
	   @Override public void windowDeactivated(WindowEvent evt) { System.out.println("Window Deactivated"); }
}
