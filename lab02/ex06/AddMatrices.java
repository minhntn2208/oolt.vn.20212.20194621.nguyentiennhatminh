package ex06;	
import java.util.Scanner;
import java.util.Arrays;
public class AddMatrices {
	public static void printMatrix(int mat[][])
    {
		System.out.print("[");
        for (int i = 0; i < mat.length; i++) {
        	System.out.print("[");
            for (int j = 0; j < mat[i].length; j++) {
                System.out.print(mat[i][j] + ",");
            }
            System.out.print("]");
        }
        System.out.print("]\n");
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int m,n;
		System.out.println("Nhap so chieu cua ma tran MxN ");
		System.out.println("M = ");
		m = sc.nextInt();
		System.out.println("N = ");
		n = sc.nextInt();
		int[][] mat1 = new int[m][n];
		int[][] mat2 = new int[m][n];
		int[][] sum = new int[m][n];
		for(int i = 0; i < m; i++) {
			for(int j = 0; j < n; j++) {
				System.out.println("mat1["+i+"][" +j+"] = ");
				mat1[i][j] = sc.nextInt();
			}
		}
		for(int i = 0; i < m; i++) {
			for(int j = 0; j < n; j++) {
				System.out.println("mat2["+i+"][" +j+"] = ");
				mat2[i][j] = sc.nextInt();
			}
		}
		sc.close();
		System.out.print("mat1: "); printMatrix(mat1); 
		System.out.print("mat2: "); printMatrix(mat2);
		for(int i = 0; i < m; i++) {
			for(int j = 0; j < n; j++) {
				sum[i][j] = mat1[i][j] + mat2[i][j];
			}
		}
		System.out.print("Sum of the two matrices: "); printMatrix(sum);
	}

}
