package ex04;
import java.util.Scanner;
import java.util.Arrays;
public class DaysOfMonth {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Nhap nam: ");
		int year = sc.nextInt();
		while(year < 0) {
			System.out.println("Nhap sai dinh dang nam, vui long nhap lai:  ");
			year = sc.nextInt();
		}
		String[] FullMonth = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
		String[] Month3Lecter = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"};
		String[] MonthAbbr = {"Jan.", "Feb.", "Mar.", "Apr.", "May.", "Jun.", "Jul.","Aug.", "Sep.", "Oct.", "Nov.", "Dec."};
		String[] Month = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
		int[] DayOfMonth = {31,28,31,30,31,30,31,31,30,31,30,31};
		System.out.println("Nhap thang: ");
		sc.nextLine();
		String month = sc.nextLine();
		while(!Arrays.asList(FullMonth).contains(month) && 
				!Arrays.asList(Month3Lecter).contains(month) &&
				!Arrays.asList(MonthAbbr).contains(month) &&
				!Arrays.asList(Month).contains(month)) {
			System.out.println("Nhap sai dinh dang thang, vui long nhap lai:  ");
			month = sc.nextLine();
		}
		int index, days;
		if(Arrays.asList(FullMonth).contains(month)) {index = Arrays.asList(FullMonth).indexOf(month);}
		else if(Arrays.asList(Month3Lecter).contains(month)) {index = Arrays.asList(Month3Lecter).indexOf(month);}
		else if(Arrays.asList(MonthAbbr).contains(month)) {index = Arrays.asList(MonthAbbr).indexOf(month);}
		else {index = Arrays.asList(Month).indexOf(month);}
		days = DayOfMonth[index];
		if( (year % 4 == 0) && (year % 100 != 0)  || (year % 400 == 0)){
			if(index == 1) {days = days+1;}
			System.out.println("Co: "+ days + " ngay");
		}
		else {System.out.println("Co: "+ days + " ngay");}
	}

}
