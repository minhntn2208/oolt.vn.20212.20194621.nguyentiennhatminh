package ex05;
import java.util.Scanner;
import java.util.Arrays;
public class SortAndCalculate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int length;
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap so phan tu cua mang: ");
		length = sc.nextInt();
		int temp = 0, sum = 0;     
        float avg = 0.0f;
		int[] arr = new int[length];
		for(int cnt = 0; cnt < length; cnt ++) {
			System.out.println("Nhap phan tu thu " + (cnt+1));
			arr[cnt] = sc.nextInt();
			sum += arr[cnt];
		}
		avg = (float)sum/length;
		sc.close();   
        for (int i = 0; i <arr.length; i++) {     
          for (int j = i+1; j <arr.length; j++) {     
              if(arr[i] >arr[j]) {     
                 temp = arr[i];    
                 arr[i] = arr[j];    
                 arr[j] = temp;    
               }     
            }     
        }    
		System.out.println(Arrays.toString(arr));
		System.out.println("Sum of the array is: " + sum + " and average value is: "+ avg);
	}

}
