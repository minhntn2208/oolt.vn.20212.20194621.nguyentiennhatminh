package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.order.Order;
import hust.soict.hedspi.aims.utils.MyDate;

public class Aims {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Star War", "SF", "George Lucas", 124, 24.95f);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Turning Red", "ComingOfAge", "DomeShi", 100, 30.5f);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Eternals","Superhero","Chloe Zhao",180,50.5f);
		
		Order anOrder1 = new Order(new MyDate(22,8,2022));
		anOrder1.addDigitalVideoDisc(dvd1);
		anOrder1.addDigitalVideoDisc(dvd2);
		anOrder1.addDigitalVideoDisc(dvd3);
		anOrder1.printOrder();
		Order anOrder2 = new Order(new MyDate(22,8,2022));
		Order anOrder3 = new Order(new MyDate(22,8,2022));
		Order anOrder4 = new Order(new MyDate(22,8,2022));
		Order anOrder5 = new Order(new MyDate(22,8,2022));
		//Uncomment to see what if the numberOrder exceed 5
		//Order anOrder6 = new Order(new MyDate(22,8,2022));
		
		System.out.print(dvd1.search("stars"));
	}

}
