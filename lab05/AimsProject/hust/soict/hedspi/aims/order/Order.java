package hust.soict.hedspi.aims.order;

import java.util.Random;

import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.utils.MyDate;

public class Order {
	//const maxOrder
	public static final int MAX_NUMBERS_ORDERED = 10;
	//array of dvds
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered = 0;
	private MyDate dateOrdered;
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;
	//get / set
	public Order(MyDate date){
		if(this.nbOrders < MAX_LIMITTED_ORDERS) {
			this.dateOrdered = date;
			this.nbOrders++;
		}else {
			throw new ArithmeticException("Exceed the number of orders can created");   
		}
	}
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	//
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(this.qtyOrdered == MAX_NUMBERS_ORDERED)
			System.out.println("The order is almost full");
		else {
			this.itemsOrdered[qtyOrdered] = disc;
			qtyOrdered++;
			System.out.println("The disc has been added");
			System.out.println("Total disc: " + this.qtyOrdered);
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc discs[]) {
		if(this.qtyOrdered == MAX_NUMBERS_ORDERED || this.qtyOrdered + discs.length > MAX_NUMBERS_ORDERED) {
			System.out.println("The order is almost full");
		}
		else {
			for(int i = 0; i < discs.length; i++) {            //for(DigitalVideoDisc disc : discs)
				this.addDigitalVideoDisc(discs[i]);  
			}
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc1, DigitalVideoDisc disc2) {
		this.addDigitalVideoDisc(disc1);
		this.addDigitalVideoDisc(disc2);
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		//check if empty
		boolean check = false;
		if(qtyOrdered != 0) {
			for(int i = 0; i < this.qtyOrdered; i++) {
				if(itemsOrdered[i] == disc) {
					check = true;
				}
				if(check == true) {
					itemsOrdered[i] = itemsOrdered[i+1];
				}
			}
			if(check == true) {
				qtyOrdered--;
			}
			else {
				System.out.println("Found no such digital video disc in the order");
			}
		}
		else {
			System.out.println("Have no disc in the order");
		}
	}
	public float totalCost() {
		float total = 0.0f;
		for(int i = 0; i < this.qtyOrdered; i++) {
			total += itemsOrdered[i].getCost();
		}
		return total;
	}
	public void printOrder() {
		System.out.println("********************Order*********************");
		System.out.print("Date: ");
		this.dateOrdered.print();
		for(int i = 0; i < this.qtyOrdered; i++) {
			System.out.print("\n" + (i+1) + ".");
			this.itemsOrdered[i].printInfo2();
		}
		System.out.println("\nTotal cost: " + totalCost());
		System.out.println("**********************************************");
		
	}
	
	public DigitalVideoDisc getALuckyItem() {
		Random rd = new Random();
		int luckynumber = rd.nextInt(this.qtyOrdered);
		this.itemsOrdered[luckynumber].setCost(0);
		return this.itemsOrdered[luckynumber];
	}
}
