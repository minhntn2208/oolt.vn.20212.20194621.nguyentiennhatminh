package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.order.Order;
import hust.soict.hedspi.aims.utils.MyDate;

public class DiskTest {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Star War", "SF", "George Lucas", 124, 24.95f);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Turning Red", "ComingOfAge", "DomeShi", 100, 30.5f);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Eternals","Superhero","Chloe Zhao",180,50.5f);
		
		Order anOrder1 = new Order(new MyDate(22,8,2022));
		anOrder1.addDigitalVideoDisc(dvd1);
		anOrder1.addDigitalVideoDisc(dvd2);
		anOrder1.addDigitalVideoDisc(dvd3);
			
		System.out.println(dvd1.search("stars"));
		System.out.println(dvd1.search("star"));
		DigitalVideoDisc dvd4 = anOrder1.getALuckyItem();
		dvd4.printInfo();
	}
}
