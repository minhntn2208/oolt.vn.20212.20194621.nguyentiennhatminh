package hust.soict.hedspi.test.disc;

import hust.soict.hedspi.aims.disc.DigitalVideoDisc;

public class TestPassingParameter {
	public static void swap(DigitalVideoDisc disc1, DigitalVideoDisc disc2) {
		DigitalVideoDisc temp = disc1;
		disc1 = disc2;
		disc2 = temp;
	}
	public static void changeTitle(DigitalVideoDisc disc1, String title) {
		String oldTitle = disc1.getTitle();
		disc1.setTitle(title);
		disc1 = new DigitalVideoDisc(oldTitle);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Star War", "SF", "George Lucas", 124, 24.95f);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Turning Red", "ComingOfAge", "DomeShi", 100, 30.5f);
		swap(dvd1,dvd2);
		dvd1.printInfo();
		changeTitle(dvd1, dvd2.getTitle());
		dvd1.printInfo();
	}

}
