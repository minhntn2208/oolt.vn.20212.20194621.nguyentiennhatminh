package hust.soict.hedspi.gui.swing;

import javax.swing.*;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.Track;
import hust.soict.hedspi.aims.PlayerException;
import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.order.Order;
import hust.soict.hedspi.aims.utils.MyDate;
import java.util.Scanner;

import java.io.PrintStream;
import hust.soict.hedspi.gui.swing.TextAreaOutputStream;
public class SwingAimGUI extends JFrame {	
	private int count;
	private CompactDisc disc;
	Order newOrder = null;
    public SwingAimGUI() {
    	JLabel labelSelectOption = new JLabel("Please select the option from 0-5");
        JLabel labelItemType = new JLabel("Please select the item type Book/CompactDisk/DigitalVideoDisk");
        JLabel labelBookInfo = new JLabel("Please insert the information of the book");
        JLabel labelCDInfo = new JLabel("Please insert the information of the compact disk");
        JLabel labelDVDInfo = new JLabel("Please insert the information of the DVD");
        JLabel labelTrackInfo = new JLabel("Please insert the information of track");
        JLabel labelRemoveID = new JLabel("Please select the item id you want to remove");
        JLabel labelOrderTotal = new JLabel("Order Total: ");
        JLabel labelDisplay = new JLabel("List of the item ");
        JLabel labelPlay = new JLabel("Select the item you want to play");
        
        
        JTextField textEnterOption = new JTextField(5);
        JTextField textEnterTypeOption = new JTextField(15);
        JTextField textEnterIDRemoved = new JTextField(5);
        JTextField textEnterIDPlayed = new JTextField(5);
        //Book
        JLabel labelBookTitle =  new JLabel("Enter title");
        JLabel labelBookCategory =  new JLabel("Enter category");
        JLabel labelBookCost =  new JLabel("Enter cost");
        
        JTextField textBookTitle = new JTextField(15);
        JTextField textBookCategory = new JTextField(15);
        JTextField textBookCost = new JTextField(5);
        
        //Compact Disc
        JLabel labelCDTitle =  new JLabel("Enter title");
        JLabel labelCDCategory =  new JLabel("Enter category");
        JLabel labelCDCost =  new JLabel("Enter cost");
        JLabel labelCDTrackNum =  new JLabel("Enter number of track");
        
        JTextField textCDTitle = new JTextField(15);
        JTextField textCDCategory = new JTextField(15);
        JTextField textCDCost = new JTextField(5);
        JTextField textCDTrackNum = new JTextField(5);
        //DVD
        JLabel labelDVDTitle =  new JLabel("Enter title");
        JLabel labelDVDCategory =  new JLabel("Enter category");
        JLabel labelDVDCost =  new JLabel("Enter cost");
        JLabel labelDVDDirector =  new JLabel("Enter director name");
        JLabel labelDVDLength =  new JLabel("Enter length");
        
        JTextField textDVDTitle = new JTextField(15);
        JTextField textDVDCategory = new JTextField(15);
        JTextField textDVDCost = new JTextField(5);
        JTextField textDVDDirector = new JTextField(15);
        JTextField textDVDLength = new JTextField(5);
        
        //Track
        JLabel labelTrackTitle =  new JLabel("Enter title");
        JLabel labelTrackLength =  new JLabel("Enter length");
        
        
        JTextField textTrackTitle = new JTextField(15);
        JTextField textTrackLength = new JTextField(5);
        
        JTextArea textDisplay = new JTextArea();
        JTextArea textDisplay2 = new JTextArea();
        
        
        JButton buttonConfirm = new JButton("OK");
        
        JButton buttonBookConfirm = new JButton("Ok");
        JButton buttonCDConfirm = new JButton("Next");
        JButton buttonDVDConfirm = new JButton("Ok");
        JButton buttonTrackConfirm = new JButton("Add");
        
        JButton buttonTypeChoose = new JButton("Choose");
        JButton buttonAddItem = new JButton("Add Item to Order");
        JButton buttonRemove = new JButton("Remove");
        JButton buttonBack = new JButton("Back");
        JButton buttonPlay = new JButton("Play");
        
    	this.setVisible(true);
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
    	JPanel panelMain = new JPanel();
        JPanel panelOption = new JPanel();
        JPanel panelItemType = new JPanel();
        JPanel panelRemove = new JPanel();
        
        JPanel panelBookInfo = new JPanel();
        JPanel panelCDInfo = new JPanel();
        JPanel panelDVDInfo = new JPanel();
        JPanel panelTrackInfo = new JPanel();
        
        JPanel panelButtons = new JPanel();
        JPanel panelOrderInfo = new JPanel();
        JPanel panelDisplay = new JPanel();
        JPanel panelPlay = new JPanel();
        
        panelMain.setLayout(new BoxLayout(panelMain, BoxLayout.Y_AXIS));
        panelOption.setLayout(new BoxLayout(panelOption, BoxLayout.Y_AXIS));
        panelItemType.setLayout(new BoxLayout(panelItemType, BoxLayout.Y_AXIS));
        panelRemove.setLayout(new BoxLayout(panelRemove, BoxLayout.Y_AXIS));
        panelBookInfo.setLayout(new BoxLayout(panelBookInfo, BoxLayout.Y_AXIS));
        panelCDInfo.setLayout(new BoxLayout(panelCDInfo, BoxLayout.Y_AXIS));
        panelDVDInfo.setLayout(new BoxLayout(panelDVDInfo, BoxLayout.Y_AXIS));
        panelTrackInfo.setLayout(new BoxLayout(panelTrackInfo, BoxLayout.Y_AXIS));
        panelDisplay.setLayout(new BoxLayout(panelDisplay, BoxLayout.Y_AXIS));
        panelPlay.setLayout(new BoxLayout(panelPlay, BoxLayout.Y_AXIS));
        
        //display
        JTextArea textArea = new JTextArea();
        TextAreaOutputStream taOutputStream = new TextAreaOutputStream(
              textArea, "Test");
        
       
        textDisplay.setText("Order Management Application:\n"+
				"--------------------------------\n" +
				"1. Create new order\n"+
				"2. Add item to the order\n"+
				"3. Delete item by id\n"+
				"4. Display the items list of order\n"+
				"5. Play\n"+
				"0. Exit"+
				"--------------------------------\n"+
				"Please choose a number: 0-1-2-3-4-5");
        
        textDisplay2.setText(
				"1. Book\n" +
				"2. Compact Disc\n" +
				"3. Digital Video Disk"
			);
        
        
        Scanner sc = new Scanner(System.in);
		
        
		panelOption.add(labelSelectOption);
		panelOption.add(textDisplay);
		panelOption.add(textEnterOption);
		panelOption.add(buttonConfirm);
		panelOption.setVisible(true);
		
        panelItemType.add(labelItemType);
        panelItemType.add(textDisplay2);
        panelItemType.add(textEnterTypeOption);
        panelItemType.add(buttonTypeChoose);
        panelItemType.setVisible(false);
        

        panelBookInfo.add(labelBookInfo);
        panelBookInfo.add(labelBookTitle);
        panelBookInfo.add(textBookTitle);
        panelBookInfo.add(labelBookCategory);
        panelBookInfo.add(textBookCategory);
        panelBookInfo.add(labelBookCost);
        panelBookInfo.add(textBookCost);
        panelBookInfo.add(buttonBookConfirm);
        panelBookInfo.setVisible(false);
        
        panelCDInfo.add(labelCDInfo);
        panelCDInfo.add(labelCDTitle);
        panelCDInfo.add(textCDTitle);
        panelCDInfo.add(labelCDCategory);
        panelCDInfo.add(textCDCategory);
        panelCDInfo.add(labelCDCost);
        panelCDInfo.add(textCDCost);
        panelCDInfo.add(labelCDTrackNum);
        panelCDInfo.add(textCDTrackNum);

        
        panelCDInfo.add(buttonCDConfirm);
        panelCDInfo.setVisible(false);
        
        panelDVDInfo.add(labelDVDInfo);
        panelDVDInfo.add(labelDVDTitle);
        panelDVDInfo.add(textDVDTitle);
        panelDVDInfo.add(labelDVDCategory);
        panelDVDInfo.add(textDVDCategory);
        panelDVDInfo.add(labelDVDCost);
        panelDVDInfo.add(textDVDCost);
        panelDVDInfo.add(labelDVDDirector);
        panelDVDInfo.add(textDVDDirector);
        panelDVDInfo.add(labelDVDLength);
        panelDVDInfo.add(textDVDLength);
        panelDVDInfo.add(buttonDVDConfirm);
        panelDVDInfo.setVisible(false);
        
        panelTrackInfo.add(labelTrackInfo);
        panelTrackInfo.add(labelTrackTitle);
        panelTrackInfo.add(textTrackTitle);
        panelTrackInfo.add(labelTrackLength);
        panelTrackInfo.add(textTrackLength);
        panelTrackInfo.add(buttonTrackConfirm);
        panelTrackInfo.setVisible(false);
        
        panelRemove.add(labelRemoveID);
        panelRemove.add(textEnterIDRemoved);
        panelRemove.add(buttonRemove);
        panelRemove.setVisible(false);
        
        panelDisplay.add(labelDisplay);
        panelDisplay.add(textArea);
        panelDisplay.add(buttonBack);
        panelDisplay.setVisible(false);
        
        panelPlay.add(labelPlay);
        panelPlay.add(textEnterIDPlayed);
        panelPlay.add(buttonPlay);
        panelPlay.setVisible(false);
        
        this.getContentPane().add(panelMain);
        panelMain.add(panelOption);
        panelMain.add(panelItemType);
        panelMain.add(panelBookInfo);
        panelMain.add(panelCDInfo);
        panelMain.add(panelDVDInfo);
        panelMain.add(panelTrackInfo);
        panelMain.add(panelRemove);
        panelMain.add(panelDisplay);
        panelMain.add(panelPlay);
        //panelMain.add(buttonConfirm);

        this.setSize(600, 300);
		

        buttonConfirm.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e1) {
						Media media;
						int choice = Integer.parseInt(textEnterOption.getText());
						textEnterOption.setText(""); 
						switch(choice) {
						case 1:
							newOrder = new Order();
							textEnterOption.setText("A new order have been created");
							break;
						case 2:
							if(newOrder != null) {

								panelOption.setVisible(false);
								panelItemType.setVisible(true);
						
							}
							else {
								textEnterOption.setText("No order is created. Please enter 1");
							}
							break;
						case 3:
							panelOption.setVisible(false);
							panelRemove.setVisible(true);
							break;
						case 4:
							panelOption.setVisible(false);
							panelDisplay.setVisible(true);
							System.setOut(new PrintStream(taOutputStream));
							newOrder.printOrder();
							break;
						case 5:
							panelOption.setVisible(false);
							panelPlay.setVisible(true);
							break;
						case 0:
							break;
						default:
							textEnterOption.setText("Please enter an option from 0-5");
							break;
						}
					}
				}
		);
        
        
        buttonTypeChoose.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e2) {
						int choice2 = Integer.parseInt(textEnterTypeOption.getText());
						switch(choice2) {
						case 1: 
							panelBookInfo.setVisible(true);
							panelItemType.setVisible(false);
							break;
						case 2: 
							panelCDInfo.setVisible(true);
							panelItemType.setVisible(false);
							break;
						case 3: 
							panelDVDInfo.setVisible(true);
							panelItemType.setVisible(false);
							break;
						default: 
							textEnterTypeOption.setText("Please insert an option from 1-3");
							break;
						}
					}
				}
		);
        
        buttonBookConfirm.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e2) {

						String title = textBookTitle.getText();
				        String category = textBookCategory.getText();
				        float cost = Float.parseFloat(textBookCost.getText());
				        
						Media media = new Book(title, category, cost);
						newOrder.addMedia(media);
						panelBookInfo.setVisible(false);
						panelOption.setVisible(true);
					}
				}
		);
        
        buttonCDConfirm.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e2) {
						count = 0;
						String title = textCDTitle.getText();
				        String category = textCDCategory.getText();
				        float cost = Float.parseFloat(textCDCost.getText());
				        
				        count = Integer.parseInt(textCDTrackNum.getText());
				    
						Media media = new CompactDisc(title, category, cost);
						disc = (CompactDisc) media;
						
						
//						newOrder.addMedia(disc);
						panelCDInfo.setVisible(false);
						panelTrackInfo.setVisible(true);
					}
				}
		);
        
        buttonDVDConfirm.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e2) {
						String title = textDVDTitle.getText();
				        String category = textDVDCategory.getText();
				        float cost = Float.parseFloat(textDVDCost.getText());
				        String director = textDVDDirector.getText();
						int length = Integer.parseInt(textDVDLength.getText());
						
						Media media = new DigitalVideoDisc(title, category, director,length, cost);
						newOrder.addMedia(media);
						panelDVDInfo.setVisible(false);
						panelOption.setVisible(true);
					}
				}
		);
        
        buttonTrackConfirm.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e2) { 
						String title = textTrackTitle.getText();
						int	length = Integer.parseInt(textTrackLength.getText()); 
						Track track = new Track(title, length);
						disc.addTrack(track);
						textTrackTitle.setText("");
						textTrackLength.setText("");
						count--;
						if(count == 0) {
							newOrder.addMedia(disc);
							panelTrackInfo.setVisible(false);
							panelOption.setVisible(true);
						}
					}
				}
		);
        
        buttonRemove.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e3) { 
						int id = Integer.parseInt(textEnterIDRemoved.getText());
						newOrder.removeMedia(id);
						panelRemove.setVisible(false);
						panelOption.setVisible(true);
					}
				}
		);
        
        buttonBack.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e3) { 
						
						panelDisplay.setVisible(false);
						panelOption.setVisible(true);
					}
				}
		);
        
        buttonPlay.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e3) { 
						
						int id2 = Integer.parseInt(textEnterIDPlayed.getText());
						Media media2 = newOrder.getMedia(id2);
						if(media2 instanceof CompactDisc) {
							CompactDisc disc1 = (CompactDisc) media2;
							try {
								disc1.play();
							}catch (PlayerException e) {
								System.out.println(e.getMessage());
							}
							
						}else if( media2 instanceof DigitalVideoDisc) {
							DigitalVideoDisc disc2 = (DigitalVideoDisc) media2;
							try {
								disc2.play();
							}catch (PlayerException e) {
								System.out.println(e.getMessage());
							}
						}
						
						panelPlay.setVisible(false);
						panelDisplay.setVisible(true);
					}
				}
		);
    }
}
