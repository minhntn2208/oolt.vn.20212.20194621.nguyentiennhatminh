package hust.soict.hedspi.aims.media;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.util.Collections;
public class Book extends Media{
	private List<String> authors = new ArrayList<String>();
	private String content = new String();
    private List<String> contenTokens = new ArrayList<String>();
    private Map<String,Integer> wordFrequency;
    public void setContent(String content) {
        this.content = content;
        wordFrequency = new HashMap<String,Integer>();
        processContent();
    }   
    public void processContent()
    {
        String contents [] = this.content.split(" ");
        for(int i= 0; i< contents.length; i++)
        {
            if(!contenTokens.contains(contents[i]))
            	contenTokens.add(contents[i]);
            if(wordFrequency.containsKey(contents[i]))
            	wordFrequency.replace(contents[i], wordFrequency.get(contents[i])+1);
            else wordFrequency.put(contents[i], 1);
        }
        
    }
    
	public Book() {}
	Book(String title){
		super(title);
	}
	Book(String title, String category){
		super(title,category);
	}
	public Book(String title, String category, float cost){
		super(title,category,cost);
	}
	public Book(String title, String category, List<String> authors){
		super(title,category);
		this.authors = authors;
	}
	public void addAuthor(String authorName) {
		if(!(authors.contains(authorName))) {
			authors.add(authorName);
		}
	}
	
	public void removeAuthor(String authorName) {
		if((authors.contains(authorName))) {
			authors.remove(authorName);
		}
	}
	
	public String toString() {
        
       for(String author : authors)
       {
           System.out.println(author);
       }
        System.out.println("CONTENT: ");
        
        Collections.sort((List)contenTokens);
        java.util.Iterator interator = contenTokens.iterator();
        while(interator.hasNext())
        {
            String str = (String)interator.next();
            System.out.println(str+" - "+ wordFrequency.get(str));
        }
       
      
        return null;
      
    }
	
	public void printInfo() {
		System.out.print("Book" + " - ");
		System.out.print(this.title + " - ");
		System.out.print(this.category + " - ");
		System.out.print(this.id + " - ");
		System.out.println(this.cost);
	}
}