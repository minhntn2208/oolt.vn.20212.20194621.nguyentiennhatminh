package hust.soict.hedspi.aims.media;
import java.util.ArrayList;
import hust.soict.hedspi.aims.PlayerException;
public class CompactDisc extends Disc implements Playable{
	private String artist;
	private ArrayList<Track> tracks = new ArrayList<>();
	public CompactDisc() {
		// TODO Auto-generated constructor stub
	}
	public CompactDisc(String tittle, String category, float cost) {
		super(tittle, category, cost);
	}
	public String getArtist() {
		return artist;
	}
	public int getLength(ArrayList<Track> tracks) {
		int sumLength = 0;
		for(int i = 0; i< tracks.size(); i++) {
			sumLength += tracks.get(i).getLength();
		}
		super.length = sumLength;
		return super.length;
	}
	public int getLength() {
		return getLength(this.tracks);
	}
	public void addTrack(Track track) {
		if(tracks.contains(track)) {
			System.out.println("Already exist");
		}
		else {
			this.tracks.add(track);
		}
	}
	
	public void removeTrack(Track track) {
		if(tracks.contains(track)) {
			this.tracks.remove(track);
		}
		else {
			System.out.println("Not exist");
		}
	}
	public void play() throws PlayerException {
		if(this.getLength() <= 0) {
			System.err.println("ERROR: CD length is 0");
			throw (new PlayerException());
		}
		
		System.out.println("Playing CD: " + this.getTitle());
		System.out.println("CD length: " + this.getLength());
		
		java.util.Iterator iter = tracks.iterator();
		Track nextTrack;
		
		while(iter.hasNext()) {
			nextTrack = (Track) iter.next();
			try {
				nextTrack.play();
			} catch (PlayerException e) {
				e.printStackTrace();
			}
		}
		
//		System.out.println("Artist: " + this.getArtist());
//		for(int i = 0; i<tracks.size(); i++) {
//			tracks.get(i).play();
//		}
	}
//	public int compareTo(Object o) {
//		return this.artist.compareTo(((CompactDisc)o).getArtist());
//	}
	public void printInfo() {
		System.out.print("CD" + " - ");
		System.out.print(this.title + " - ");
		System.out.print(this.category + " - ");
		System.out.print(this.id + " - ");
		System.out.println(this.cost);
	}
}
