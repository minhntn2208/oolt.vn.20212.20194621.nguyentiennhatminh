package hust.soict.hedspi.aims.media;
import hust.soict.hedspi.aims.PlayerException;
public class Track implements Playable, Comparable{
	private String title;
	private int length;
	public Track() {
		// TODO Auto-generated constructor stub
		this.title = "";
		this.length = 0;
	}
	public Track(String title, int length) {
		// TODO Auto-generated constructor stub
		this.title = title;
		this.length = length;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public void play() throws PlayerException{
		if(this.getLength() <= 0) {
			System.err.println("ERROR: Track length is 0");
			throw (new PlayerException());
		}
		System.out.println("Play Track: " + this.getTitle());
		System.out.println("Track length: " + this.getLength());
	}
	public boolean equals(Object o) {
		if(o == null) return false;
		if(o instanceof Track) {
			Track track2 = (Track)o;
			return (this.getTitle().equals(track2.getTitle())) && (this.length == track2.length);
		}
		return false;
	}
	
	public int compareTo(Object o) {
		return this.title.compareTo(((Track)o).getTitle());
	}
}
