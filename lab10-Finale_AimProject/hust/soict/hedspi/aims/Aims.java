package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.gui.swing.SwingAimGUI;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.media.Track;
import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.order.Order;
import hust.soict.hedspi.aims.utils.MyDate;
import java.util.Scanner;
public class Aims {
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("5. Play");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4-5");
	}
	
	public static void showItemType() {
		System.out.println("Please choose the type of item you want: ");
		System.out.println("1. Book");
		System.out.println("2. Compact Disc");
		System.out.println("3. Digital Video Disc");
	}
	public static void main(String[] args) {
//		Thread memCon = new Thread(new MemoryDaemon(), "Memory Observation");
//		memCon.setDaemon(true);
//		memCon.start();
//		// TODO Auto-generated method stub
//		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Star War", "SF", "George Lucas", 124, 24.95f);
//		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Turning Red", "ComingOfAge", "DomeShi", 100, 30.5f);
//		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Eternals","Superhero","Chloe Zhao",180,50.5f);
		new SwingAimGUI();
//		int choice;
//		Scanner sc = new Scanner(System.in);
//		Order newOrder = null;
//		do {
//			showMenu();
//			choice = sc.nextInt();
//			switch(choice) {
//			case 1:
//				newOrder = new Order();
//				break;
//			case 2:
//				if(newOrder != null) {
//					int choice2;
//					Media media;
//					showItemType();
//					choice2 = sc.nextInt();
//					
//					System.out.println("Insert the tittle: ");
//					String title = sc.next();
//					sc.nextLine();
//					System.out.println("Insert the category: ");
//					String category = sc.next();
//					System.out.println("Insert item price: ");
//					float cost = sc.nextFloat();
//					
//					switch(choice2) {
//					case 1:
//						media = new Book(title, category, cost);
//						newOrder.addMedia(media);
//						break;
//					case 2:
//						System.out.println("Insert the number of track: ");
//						int num = sc.nextInt();
//						media = new CompactDisc(title, category, cost);
//						CompactDisc disc = (CompactDisc) media;
//						for(int i = 0; i< num; i++) {
//							System.out.println("Insert the title of track " + (i+1));
//							String trackTitle = sc.next();
//							System.out.println("Insert the length of track " + (i+1));
//							int trackLength = sc.nextInt();
//							Track track = new Track(trackTitle, trackLength);
//							disc.addTrack(track);
//						}
//						newOrder.addMedia(disc);
//						break;
//					case 3:
//						System.out.println("Insert the length of the video: ");
//						int length = sc.nextInt();
//						System.out.println("Insert the name of the director: ");
//						String director = sc.next();
//						media = new DigitalVideoDisc(title, category, director,length, cost);
//						newOrder.addMedia(media);
//						break;
//					default:
//						break;
//					}
//				}
//				else {
//					System.out.println("No order have been created");
//				}
//				break;
//			case 3:
//				System.out.println("Please insert the item id you want to remove: ");
//				int id = sc.nextInt();
//				newOrder.removeMedia(id);
//				break;
//			case 4:
//				newOrder.printOrder();
//				break;
//			case 5:
//				System.out.println("Please insert the item id you want to play: ");
//				int id2 = sc.nextInt();
//				Media media2 = newOrder.getMedia(id2);
//				if(media2 instanceof CompactDisc) {
//					CompactDisc disc1 = (CompactDisc) media2;
//					try {
//						disc1.play();
//					}catch (PlayerException e) {
//						System.out.println(e.getMessage());
//					}
//					
//				}else if( media2 instanceof DigitalVideoDisc) {
//					DigitalVideoDisc disc2 = (DigitalVideoDisc) media2;
//					try {
//						disc2.play();
//					}catch (PlayerException e) {
//						System.out.println(e.getMessage());
//					}
//				}
//				break;
//			case 0:
//				break;
//			default:
//				break;
//			}
//		}while(choice != 0);
//		System.out.println("Exit");
//		sc.close();
//	}
	
	
	
	
	
	
	
//		java.util.Collection collection = new java.util.ArrayList(); 
//		collection.add(dvd2);
//		collection.add(dvd1);
//		collection.add(dvd3);
//		
//		java.util.Iterator iterator = collection.iterator();
//		System.out.println("---------------------");
//		System.out.println("The DVDs currently in the order are: ");
//		while(iterator.hasNext()) {
//			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
//		}
//		java.util.Collections.sort((java.util.List)collection);
//		
//		iterator = collection.iterator();
//		
//		System.out.println("---------------------");
//		System.out.println("The DVDsin sorted order are: ");
//		while(iterator.hasNext()) {
//			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
//		}
//		System.out.println("---------------------");
	}
}
