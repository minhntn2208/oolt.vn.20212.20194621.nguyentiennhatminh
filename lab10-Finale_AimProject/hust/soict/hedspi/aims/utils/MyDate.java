package hust.soict.hedspi.aims.utils;

import java.util.Scanner;

public class MyDate {
	//Attribute
		private int day;
		private int month;
		private int year;
		//Method
		//Constructor
		public MyDate() {
			this.day = 1;
			this.month = 1;
			this.year = 1900;
		}
		public MyDate(int day, int month, int year) {
			this.day = day;
			this.month = month;
			this.year = year;
		}
		public MyDate(String  day,  String  month, String year) {
			 String[] days ={
			        "first", "second", "third", "fourth", "fifth", "sixth","seventh","eighth","ninth","tenth","eleventh","twelfth","thirteenth","fourteenth",
			        "fifteenth","sixteenth","seventeenth","eighteenth","nineteenth","twenty","twenty first","twenty-second","twenty third","twenty fourth",
			        "twenty fifth","twenty-sixth","twenty-seventh","twenty-eighth","twenty-eighth","Thirtieth","Thirty-first"
			          };
			 String[] FullMonth = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
			 String[] years = {	"thousand","one","two","three","four","five","six","seven","eight","nine","ten",
					 "eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen",	
					 "twenty","thirty","fourty","fithty","sixty","seventy", "eighty","ninety"};
			 for(int i = 0; i < days.length; i++) {
					if(day.equals(days[i])) {
						this.day = i + 1;
					}
				}
			 for(int i = 0; i < FullMonth.length; i++) {
					if(month.equals(FullMonth[i])) {
						this.month = i + 1;
					}
				}
			 String[] yearSplitter = year.split(" ");
			 int str_len = yearSplitter.length;
			 int first_half = 19, second_half = 0;
			 int first_second_half = 0, second_second_half = 0;
			 switch(str_len) {
			 case 2:
				for(int j = 0; j < years.length; j++) {
					if(yearSplitter[0].equals(years[j])) {
						first_half = j;
					}
					if(yearSplitter[1].equals(years[j])) {
						if(j > 20) {
							second_half = (j - 18) * 10;
						}
						else	
						second_half = j;
					}	 	 
				 }
				break;
			 case 3:
				 for(int j = 0; j < years.length; j++) {
						if(yearSplitter[0].equals(years[j])) {
							first_half = j;
						}	 
					 }
				 if(first_half < 10) {
					 for(int j = 0; j < years.length; j++) {
						if(yearSplitter[2].equals(years[j]) && j <=20) {
							second_half = j;
						}
						if(yearSplitter[2].equals(years[j]) && j > 20) {
							second_half = (j - 18) * 10;
						}
					 }
				 }
				 else {
					 for(int j = 0; j < years.length; j++) {
							if(yearSplitter[1].equals(years[j])) {
								first_second_half = j - 18;
							}
							if(yearSplitter[2].equals(years[j])) {
								second_second_half =  j;
							}
						 }
					 second_half = first_second_half*10 + second_second_half;
				 }
					break;
			 case 4:
				 for(int j = 0; j < years.length; j++) {
						if(yearSplitter[0].equals(years[j])) {
							first_half = j;
						}
						if(yearSplitter[2].equals(years[j])) {
							first_second_half = j - 18;
						}
						if(yearSplitter[3].equals(years[j])) {
							second_second_half =  j;
						}
					 }
				 second_half = first_second_half*10 + second_second_half;
					break;
			 }
			 this.year = (first_half > 10) ? first_half*100 + second_half : first_half*1000 + second_half;
			 
		}
		
		public MyDate(String date) {
			char day[] = new char[10];
			char month[] = new char[10];
			char year[] = new char[10];
			String[] FullMonth = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
			int iter = 0,iter1 = 0, iter2 = 0;
			while(date.charAt(iter) != ' ') {
				month[iter] = date.charAt(iter);
				iter++;
			}
			month[iter] = '\0';
			String monthString = new String(month).split("\0")[0];
			for(int i = 0; i < FullMonth.length; i++) {
				if(monthString.equals(FullMonth[i])) {
					this.month = i + 1;
				}
			}
			iter++;
			while(date.charAt(iter) != ' ') {
				if(date.charAt(iter)>= '0' && date.charAt(iter)<= '9') {
					day[iter1] = date.charAt(iter);
					iter1++;
				}
				iter++;
			}
			day[iter1] = '\0';
			iter++;
			this.day = Integer.parseInt(new String(day).split("\0")[0]);
			while(iter < date.length()) {
				year[iter2] = date.charAt(iter);
				iter++; iter2++;
			}
			year[iter2] = '\0';
			this.year = Integer.parseInt(new String(year).split("\0")[0]);
		}
		//setter and getter
		public int getDay() {
			return day;
		}
		public void setDay(int day) {
			if(this.month == 1 || this.month == 3 || this.month == 5 || this.month == 7 
					|| this.month == 8 || this.month == 10 || this.month == 12) {
				if(day >= 1 && day <= 31) {
					this.day = day;
				}
				else
					this.day = 1;
			}
			else if(this.month == 4 || this.month == 6 || this.month == 9 || this.month == 11){
				if(day >= 1 && day <= 30) {
					this.day = day;
				}
				else
					this.day = 1;
			}
			else {
				if( (this.year % 4 == 0) && (this.year % 100 != 0)  || (this.year % 400 == 0)) {
					if(day >= 1 && day <= 29) {
						this.day = day;
					}
					else
						this.day = 1;
				}
				else {
					if(day >= 1 && day <= 28) {
						this.day = day;
					}
					else
						this.day = 1;
				}
			}
		}
		public int getMonth() {
			return month;
		}
		public void setMonth(int month) {
			if(month >= 1 && month <= 12) {
				this.month = month;
			}
			else
				this.month = 1;
		}
		public int getYear() {
			return year;
		}
		public void setYear(int year) {
			if(year > 0) {
				this.year = year;
			}
			else
				this.year = 0;
		}
		//utils method
		public void accept() {
			Scanner sc = new Scanner(System.in);

			System.out.println("Enter the date:");
			String date = sc.nextLine();
			
			MyDate temp = new MyDate(date);
			this.day = temp.getDay();
			this.month = temp.getMonth();
			this.year = temp.getYear();
		}
		public void print() {
			String[] FullMonth = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
			String s;
			switch(this.day){
			case 1,21,31:
				s = "st";
				break;
			case 2,22:
				s = "nd";
				break;
			case 3,23:
				s = "rd";
				break;
			default:
				s = "th";
				break;
			}
			System.out.println(FullMonth[this.month-1] + " " + this.day + s + " " + this.year);
		}
		
		public void print(int format) {
			String[] Month3Lecter = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"};
			String d,m;
			if(this.day < 10) d = "0"; else d ="";
			if(this.month < 10) m = "0"; else m ="";
			switch(format) {
			case 1:
				System.out.println(this.year+"-"+ m + this.month + "-" + d + this.day);
				break;
			case 2:
				System.out.println(this.day+"/"+  this.month + "/" +  this.year);
				break;
			case 3:
				System.out.println(d + this.day+"-"+  Month3Lecter[this.month-1] + "-" +  this.year);
				break;
			case 4:
				System.out.println(Month3Lecter[this.month-1] + " " + this.day +" "+   this.year);
				break;
			default:
				System.out.println(d + this.day +"-"+ m + this.month + "-" + this.year);
				break;
			}	
		}
}
